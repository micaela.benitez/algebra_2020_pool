#ifndef BORDER_H
#define BORDER_H

#include "raylib.h"

class Border
{
private:
	Rectangle collider;
	Color color;

public:
	Border();
	~Border();

	Rectangle getCollider();
	void setCollider(Rectangle coll);

	Color getColor();
	void setColor(Color col);


};

#endif 