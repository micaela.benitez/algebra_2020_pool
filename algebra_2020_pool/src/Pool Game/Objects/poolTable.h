#ifndef POOL_TABLE_H
#define POOL_TABLE_H

#include "raylib.h"
#include "Pool Game/General/screenConfig.h"
#include "border.h"
#include "hole.h"

const Rectangle POOL_TABLE{ 0 + screenWidth / 20, 0 + screenHeight / 20, screenWidth - (screenWidth / 20 * 2), screenHeight - (screenHeight / 20 * 2) };
const int POOL_TABLE_FRAME_SIZE = 50;
const float POOL_TABLE_FRICCTION = .75f;

const int POOL_TABLE_HOLES = 6;
const Color POOL_TABLE_HOLES_COLOR = GRAY;
const Color POOL_TABLE_BORDERS_COLOR = DARKBROWN;
const Color POOL_TABLE_BACKBOARD_COLOR = DARKGREEN;
const int POOL_TABLE_BORDERS = 6;

class PoolTable
{
private:
	int height;
	int width;
	int x;
	int y;
	Hole* holes[POOL_TABLE_HOLES];
	Border* borders[POOL_TABLE_BORDERS];
	Rectangle backboard;

public:

	PoolTable();
	~PoolTable();

	int getHeight();
	void setHeight(int h);

	int getWidth();
	void setWidth(int w);

	int getX();
	void setX(int _x);

	int getY();
	void setY(int _y);

	Rectangle getBackboard();
	void setBackboard(Rectangle b);	

	void initHoles();
	Hole* getHoles(int i);

	void initBorders();
	Border* getBorders(int i);

	void init();

	void draw();


};

#endif