#include "hole.h"
#include "poolTable.h"
#include "Pool Game/General/screenConfig.h"

Hole::Hole()
{
	position = { 0, 0 };
	radius = HOLE_BASE_RADIUS;
	color = BLACK;
}

void Hole::draw()
{
	DrawCircle(static_cast<int>(getPosition().x), static_cast<int>(getPosition().y), getRadius(), getColor());
}

Vector2 Hole::getPosition()
{
	return position;
}

void Hole::setPosition(Vector2 pos)
{
	position = pos;
}

float Hole::getRadius()
{
	return radius;
}

void Hole::setRadius(float rad)
{
	radius = rad;
}

Color Hole::getColor()
{
	return color;
}

void Hole::setColor(Color col)
{
	color = col;
}