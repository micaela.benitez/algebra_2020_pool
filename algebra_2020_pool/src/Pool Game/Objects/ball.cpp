#include "ball.h"
#include "poolTable.h"
#include "Pool Game/General/screenConfig.h"
#include "Pool Game/General/customMath.h"

Ball::Ball()
{
	velocity = { 0, 0 };
	position = { 0, 0 };
	radius = BALL_BASE_RADIUS;
	texture = nullptr;
    active = false;
	color = BLACK;
}
void Ball::update()
{
	position.x += velocity.x * GetFrameTime();
	position.y += velocity.y * GetFrameTime();
	velocity.x *= BALL_FRICTION_COEFICIENT;
	velocity.y *= BALL_FRICTION_COEFICIENT;
	if(customAbs(velocity.x) < BALL_STOP_SPEED && customAbs(velocity.y) < BALL_STOP_SPEED)
	{
		velocity.x = 0;
		velocity.y = 0;
	}
}
void Ball::draw(int ballNumber)
{
    if (active)
    {
        DrawCircle(static_cast<int>(getPosition().x), static_cast<int>(getPosition().y), getRadius(), getColor());
        if (ballNumber != 0)   // Numbers
        {
            if (ballNumber != 8) DrawText(TextFormat("%i", ballNumber), static_cast<int>(getPosition().x) - 6, static_cast<int>(getPosition().y) - 8, 20, BLACK);
            else DrawText(TextFormat("%i", ballNumber), static_cast<int>(getPosition().x) - 6, static_cast<int>(getPosition().y) - 8, 20, WHITE);
        }
    }   
}
Vector2 Ball::getVelocity()
{
	return velocity;
}
void Ball::setVelocity(Vector2 vel)
{
	velocity = vel;
}
Vector2 Ball::getPosition()
{
	return position;
}
void Ball::setPosition(Vector2 pos)
{
	position = pos;
}
float Ball::getRadius()
{
	return radius;
}
void Ball::setRadius(float rad)
{
	radius = rad;
}
Texture2D* Ball::getTexture()
{
	return texture;
}
void Ball::setTexture(Texture2D* tex)
{
	texture = tex;
}
Color Ball::getColor()
{
	return color;
}
void Ball::setColor(Color col)
{
	color = col;
}
bool Ball::getActive()
{
    return active;
}
void Ball::setActive(bool act)
{
    active = act;
}

void Ball::hitBall(Vector2 mousePosition)
{
    setVelocity({ (getPosition().x - mousePosition.x) * BALL_WHITE_SHOOT_SPEED_MULTIPLIER, (getPosition().y - mousePosition.y) * BALL_WHITE_SHOOT_SPEED_MULTIPLIER });
}

void SetBallsStartingConditions(Ball* balls[16], const int BALLS_AMMOUNT)
{
    // Positions
    Vector2 aux = { POOL_TABLE.x + POOL_TABLE_FRAME_SIZE + POOL_TABLE.width * WHITE_BALL_STARTING_POSITION, screenHeight / 2 };
    balls[0]->setPosition(aux);
    aux = { POOL_TABLE.x + POOL_TABLE_FRAME_SIZE + POOL_TABLE.width * BALLS_STARTING_POSITION - 1, screenHeight / 2 };
    balls[1]->setPosition(aux);
    aux = { POOL_TABLE.x + POOL_TABLE_FRAME_SIZE + POOL_TABLE.width * BALLS_STARTING_POSITION - 1, (screenHeight / 2) + balls[1]->getRadius() * 2 + 1 };
    balls[2]->setPosition(aux);
    aux = { POOL_TABLE.x + POOL_TABLE_FRAME_SIZE + POOL_TABLE.width * BALLS_STARTING_POSITION - 1, (screenHeight / 2) + balls[1]->getRadius() * 4 + 1 };
    balls[3]->setPosition(aux);
    aux = { POOL_TABLE.x + POOL_TABLE_FRAME_SIZE + POOL_TABLE.width * BALLS_STARTING_POSITION - 1, (screenHeight / 2) - balls[1]->getRadius() * 2 - 1 };
    balls[4]->setPosition(aux);
    aux = { POOL_TABLE.x + POOL_TABLE_FRAME_SIZE + POOL_TABLE.width * BALLS_STARTING_POSITION - 1, (screenHeight / 2) - balls[1]->getRadius() * 4 - 1 };
    balls[5]->setPosition(aux);
    aux = { POOL_TABLE.x + POOL_TABLE_FRAME_SIZE + POOL_TABLE.width * BALLS_STARTING_POSITION + SQRT_THREE * balls[1]->getRadius(), (screenHeight / 2) + balls[1]->getRadius() * 3 + 1 };
    balls[11]->setPosition(aux);
    aux = { POOL_TABLE.x + POOL_TABLE_FRAME_SIZE + POOL_TABLE.width * BALLS_STARTING_POSITION + SQRT_THREE * balls[1]->getRadius(), (screenHeight / 2) + balls[1]->getRadius() + 1 };
    balls[12]->setPosition(aux);
    aux = { POOL_TABLE.x + POOL_TABLE_FRAME_SIZE + POOL_TABLE.width * BALLS_STARTING_POSITION + SQRT_THREE * balls[1]->getRadius(), (screenHeight / 2) - balls[1]->getRadius() * 3 - 1 };
    balls[10]->setPosition(aux);
    aux = { POOL_TABLE.x + POOL_TABLE_FRAME_SIZE + POOL_TABLE.width * BALLS_STARTING_POSITION + SQRT_THREE * balls[1]->getRadius(), (screenHeight / 2) - balls[1]->getRadius() - 1 };
    balls[9]->setPosition(aux);
    aux = { POOL_TABLE.x + POOL_TABLE_FRAME_SIZE + POOL_TABLE.width * BALLS_STARTING_POSITION + SQRT_THREE * balls[1]->getRadius() * 2 + 1, screenHeight / 2 };
    balls[8]->setPosition(aux);
    aux = { POOL_TABLE.x + POOL_TABLE_FRAME_SIZE + POOL_TABLE.width * BALLS_STARTING_POSITION + SQRT_THREE * balls[1]->getRadius() * 2 + 1, (screenHeight / 2) + balls[1]->getRadius() * 2 + 1 };
    balls[6]->setPosition(aux);
    aux = { POOL_TABLE.x + POOL_TABLE_FRAME_SIZE + POOL_TABLE.width * BALLS_STARTING_POSITION + SQRT_THREE * balls[1]->getRadius() * 2 + 1, (screenHeight / 2) - balls[1]->getRadius() * 2 - 1 };
    balls[7]->setPosition(aux);
    aux = { POOL_TABLE.x + POOL_TABLE_FRAME_SIZE + POOL_TABLE.width * BALLS_STARTING_POSITION + SQRT_THREE * balls[1]->getRadius() * 3 + 2, (screenHeight / 2) + balls[1]->getRadius() + 1 };
    balls[13]->setPosition(aux);
    aux = { POOL_TABLE.x + POOL_TABLE_FRAME_SIZE + POOL_TABLE.width * BALLS_STARTING_POSITION + SQRT_THREE * balls[1]->getRadius() * 3 + 2, (screenHeight / 2) - balls[1]->getRadius() - 1 };
    balls[14]->setPosition(aux);
    aux = { POOL_TABLE.x + POOL_TABLE_FRAME_SIZE + POOL_TABLE.width * BALLS_STARTING_POSITION + SQRT_THREE * balls[1]->getRadius() * 4 + 3, screenHeight / 2 };
    balls[15]->setPosition(aux);
    // Colors
    for (int i = 0; i < BALLS_AMMOUNT; i++)
    {
        balls[i]->setActive(true);
        if (i > 0 && i < 8) balls[i]->setColor(RED);
        else if (i > 8) balls[i]->setColor(BLUE);
        else if (i == 0) balls[i]->setColor(LIGHTGRAY);
        else if (i == 8) balls[i]->setColor(BLACK);
    }

}