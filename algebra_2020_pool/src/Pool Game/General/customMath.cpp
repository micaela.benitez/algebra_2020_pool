#include "customMath.h"
#include <math.h>

float customAbs(float f)
{
	if(f < 0)
	{
		f = -f;
	}
	return f;
}

float distance2D(Vector2 point1, Vector2 point2)
{
	float distanceX = point1.x - point2.x;
	float distanceY = point1.y - point2.y;
	return sqrtf((distanceX * distanceX) + (distanceY * distanceY));
}